<?php
global $woo_options;

    //API Key - see http://admin.mailchimp.com/account/api
    $apikey = $woo_options['woo_mc_api_key'];
    
    // A List Id to run examples against. use lists() to view all
    // Also, login to MC account, go to List, then List Tools, and look for the List ID entry
    $listId = $woo_options['woo_mc_list_id'];
    
    // A Campaign Id to run examples against. use campaigns() to view all
    $campaignId = '';

    //some email addresses used in the examples:
    $my_email = $woo_options['woo_contactform_email'];
   // $boss_man_email = 'INVALID@example.com';

    //just used in xml-rpc examples
    $apiUrl = 'http://api.mailchimp.com/1.3/';

?>
