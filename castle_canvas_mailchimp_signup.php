<?php  
/*
Plugin Name: Castle Canvas Mailchimp Signup
Plugin URI: http://www.wpdevelopers.org
Description: A simple integration of the mailchimp signup API (PHP MCAPI 1.3 - http://api.mailchimp.com/1.3/.) This is designed to work with Woo Theme Options and also allows adding groups

Author: Jim Camomile - wpdevelopers.org
Version: 0.1.0
*/

/*
NOTICE
this plugin requires the use of Woo Canvas. OTher woothemes with the woo theme-options may also work. The Theme-options for this plugin must be added to the
theme(or child theme) / includes/theme-options.php. See code in themeoptions_code.php for more details.
*/

function mc_api_call($type='comment'){
	global $woo_options;
	
	require_once("config.inc.php");		
	require_once("MCAPI.class.php");
		
	// grab an API Key from http://admin.mailchimp.com/account/api/
	$api = new MCAPI($apikey); 
	// List the groups
	//print_r($woo_options['woo_mc_use_groupings_home'] );
	if( (( $type == 'reg' && isset($woo_options['woo_mc_use_groupings']) && $woo_options['woo_mc_use_groupings'] == 'true'  ) || ( $type == 'home' && isset($woo_options['woo_mc_use_groupings_home']) && $woo_options['woo_mc_use_groupings_home'] == 'true'  ) ) && ( !isset($_POST['submit']) && !isset($_POST['ajax'])) ) { 
		$groupys = $api->listInterestGroupings($listId);
		if (isset($groupys) && !empty($groupys) ){
			$gcnt = 0;
			echo '<div class="mc-groups">';
			foreach ($groupys as $groupy){
				$gcnt++;
				echo '<div class="groups-title">'.$groupy['name'].'</div>';
				echo '<input name="thegroups-'.$gcnt.'" type="hidden" value="'.$groupy['name'].'" />';
				$groupitems = $groupy['groups'];
				if (isset($groupitems) && !empty($groupitems) ){
					echo '<ul id=grouplist>';
					$cnt = 0;
					foreach ( $groupitems as $group) {
						$cnt++;
						echo'<li><input id="mc-group-'.$cnt.'" class="checkbox" type="checkbox" value="'.$group['name'].'" name="group'.$cnt.'">
						<label for="mc-group-'.$cnt.'">'.$group['name'].'</label></li>';
					} // end foreach
					echo '</ul>';
				} // end  if
				
			} // end foreach
		echo '</div>'; 
		} // end if
	} // END IF

	// On Submit
	if( isset($_POST['submit']) || isset($_POST['ajax']) ){
	
		// Validation
		if(!$_POST['email']){ 
			return "No email address provided"; 
		} 
	
		if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $_POST['email'])) {
			return "Email address is invalid"; 
		}
		
		// add merge vars
		$mergeVars = ''; //array('ZIP'=>$_POST['zip']);
		
		if( isset($woo_options['woo_mc_subscribe_with_name']) && $woo_options['woo_mc_subscribe_with_name'] == 'true' ) {
			$mergeVars = array('FNAME'=>$_POST['name']);
		}

		// To Do - Add Name field if it is selected

		// Groups Merge Vars
		if( (( $type == 'reg' && $woo_options['woo_mc_use_groupings'] ) || ( $type == 'home' && $woo_options['woo_mc_use_groupings_home'] ) )  ) {
			$getgroups = $_POST;
			$mygroups = array();
			foreach ($getgroups as $key => $value){
				$gotgroup = substr($key,0,5);
				if ($gotgroup=='group'){
					$mygroups[] = $value;
				}
			}
			if ($mygroups){
				$mc_groupings_groups = implode(',',$mygroups);
				$mergeVars['GROUPINGS'] = array( 
					array( 'name' => $_POST['thegroups-1'], 'groups' => $mc_groupings_groups )
				);
					 // print_r($mc_groupings_groups);
					//exit;
			} // end if
		}	
		
		if($api->listSubscribe($listId, $_POST['email'], $mergeVars) === true) {
			// It worked!	
			return '<div class="message">Success! Check your email to confirm.</div>';
		}else{
			// An error ocurred, return error message	
			return '<div class="message"> Error: ' . $api->errorMessage .'</div>';
		}
	}
	
}

	// Merge variables are the names of all of the fields your mailing list accepts
	// Ex: first name is by default FNAME
	// You can define the names of each merge variable in Lists > click the desired list > list settings > Merge tags for personalization
	// Pass merge values to the API in an array as follows
	
	//$mergeVars = array('FNAME'=>$_POST['fname'], 'LNAME'=>$_POST['lname'],'ADDRESS'=>$_POST['address'],'CITY'=>$_POST['city'],'STATE'=>$_POST['state'],						'ZIP'=>$_POST['zip']);


function output_mc_form($type='comment'){
		global $post, $woo_options;
		$wooextra = '';
		if ($type == 'home'){$wooextra = '_home';}
		echo  '<div class="mc_signup_form '.$type.'">';
	//	echo  '<div class="mc_title">'.$woo_options['woo_subscribe_widget_title'].'</div>';
		echo  '<form class="mailchimp_signup" action="'.get_permalink().'" method="post">';
		echo  ' <fieldset>';
			  
		echo  '<div class="response">';
		echo  mc_api_call($type); 	 // loads groups or response		
		echo  '</div>';

		// use name field
		if( ( $woo_options['woo_mc_subscribe_with_name'.$wooextra]) ) {
		
			echo  '<input type="text" name="name" id="name" class="fieldinput" value="First Name" onfocus="if (this.value == \'First Name\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \'First Name\';}" />';
			
		} // end if

		$em_val = (isset($woo_options['woo_email_default_value']) && !empty($woo_options['woo_email_default_value']) ? $woo_options['woo_email_default_value'] : 'Email Address');
		echo  '<input type="text" name="email" id="email" class="fieldinput" value="'.$em_val.'" onfocus="if (this.value == \''.$em_val.'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.$em_val.'\';}"/>';
		
	//	
			
		echo  '<input type="submit" name="submit" value="'.$woo_options['woo_submit_button'.$wooextra].'" class="btn"  />';
		
		echo  '	<div class="after-mc-form">'.$woo_options['woo_text_after_signup'.$wooextra].'</div>';
		echo  ' </fieldset>';
		echo  '</form>';
		echo  '</div>';
		
		//return $output
}


//Add the Widget
include('widget-mc-subscribe.php');

//Add the Widget for home
include('widget-mc-home-subscribe.php');

// Widget CSS
if ( ! function_exists( 'mc_form_css_load' ) ) {
	function mc_form_css_load() {
		$siteurl = get_option('siteurl');
		$url = $siteurl . '/wp-content/plugins/' . basename(dirname(__FILE__)) . '/css/mcform-styles.css';
		wp_register_style( 'mcform-styles', $url);
		wp_enqueue_style('mcform-styles');
	}
}
add_action( 'wp_head', 'mc_form_css_load', 20 );

// add to comment form
/*
function add_subscribe_to_comments(){
	global $woo_options;
	if (!is_single()){
		return;
	}
	//print_r('ahhhhhhhhhhhh'.$woo_options['woo_add_to_comment_form']);exit;
	
	if( isset($woo_options['woo_add_to_comment_form']) && $woo_options['woo_add_to_comment_form'] == 'true') {
			//add_action('thesis_hook_after_comment_box',output_mc_form,20);
			add_action('comment_form','output_mc_form',5);
			add_action('comment_approved_','output_mc_form',10,1);
			add_action('comment_post', 'output_mc_form', 50, 2);
			//add_action('woo_post_after','output_mc_form',20);
		}
}

function load_add_subscribe_to_comments(){
	add_subscribe_to_comments();
}
add_action('wp_head','load_add_subscribe_to_comments',20);

*/

// Sad to have to write this but wordpress does not have a good way to eval if you are on the true home page. It often treats the home and blog page the same
if ( ! function_exists( 'is_reallyhome' ) ) {
	function is_reallyhome(){
		if ($_SERVER["REQUEST_URI"] == '/' ) { $reallyhome = true; } else {$reallyhome = false;}
		return $reallyhome;
	}
}
?>