<?php
/*


*/

if ( ! defined( 'ABSPATH' ) ) exit;
/*---------------------------------------------------------------------------------*/
/* widget */
/*---------------------------------------------------------------------------------*/
class Simple_Mailchimp_Home_Subscribe extends WP_Widget {
	var $settings = array( 'title', 'form' );

	function Simple_Mailchimp_Home_Subscribe() {
		$widget_ops = array( 'description' => 'Display Mailchimp Signup Widget for Homepage. This Widget is generally best for adding to sliders and other "landscape" oriented page elements. Form can be modified in mailchimp api plugin and form values and options can be set in Theme Options' );
		parent::WP_Widget( false, 'Castle Mailchimp Subscribe for Home Page', $widget_ops );
	}

	function widget( $args, $instance ) {
		global $woo_options;
	
		/* Provide some defaults */
		$defaults = array( 'title' => '', 'text_before_form' => '', 'text_after_form' => '');
		$instance = wp_parse_args( (array) $instance, $defaults );	
			
		extract( $args );
		extract($instance);
		
		if (!empty($title)){ 
			$title = apply_filters('widget_title', $title);
			} else {
			$title  = $woo_options['woo_subscribe_widget_title'];
			}
			
		echo $before_widget;
		// insert the title
			// insert the title
			 echo '<h3>'.$woo_options['woo_subscribe_widget_title_home'].'</h3>'; 
			 
			// insert the image
			// show the image if it is set to show and it exists
			// $woo_options['woo_subscribe_widget_iwidth'] ? $thewidth = $woo_options['woo_subscribe_widget_iwidth'] : $thewidth = 200;
			
			$thealign  = (isset($woo_options['woo_subscribe_widget_ialign_home']) && !empty($woo_options['woo_subscribe_widget_ialign_home']) ? $woo_options['woo_subscribe_widget_ialign_home'] : 'aligncenter' );
			
			$thewidth = 250;
			if ( isset( $woo_options['woo_subscribe_widget_image_home'] ) && $woo_options['woo_subscribe_widget_image_home'] != '' ) {
				$theimage = $woo_options["woo_subscribe_widget_image_home"];
				woo_image('key=image&width='.$thewidth.'&noheight=true&force=true&id=123&class=thumbnail '.$thealign.'&src='.$theimage.''); 
			}
			// end image
			
			// before text
			if(!empty($woo_options['woo_subscribe_widget_text_before_home'])) { 
				echo '<div class="text-before-form">'.$woo_options['woo_subscribe_widget_text_before_home'].'</div>';		
			}
			echo '<div class="fix"></div>';
			echo output_mc_form('home');
			echo '<div class="fix"></div>';
		
				// the after text

					if(!empty($woo_options['woo_subscribe_widget_text_after_home'])) { 
						echo '<div class="text-after-form">'.$woo_options['woo_subscribe_widget_text_after_home'].'</div>';		
					}
			
			echo '<div class="fixed"></div>';

			// end after text

		echo $after_widget; 
	}

	function update($new_instance, $old_instance) {
		$new_instance = $this->woo_enforce_defaults( $new_instance );
		return $new_instance;
	}

	function woo_enforce_defaults( $instance ) {
		$defaults = $this->woo_get_settings();
		$instance = wp_parse_args( $instance, $defaults );
		$instance['title'] = strip_tags( $instance['title'] );
		if ( '' == $instance['title'] )
			$instance['title'] = 'Sign up for our newsletter';
		return $instance;
	}

	/**
	 * Provides an array of the settings with the setting name as the key and the default value as the value
	 * This cannot be called get_settings() or it will override WP_Widget::get_settings()
	 */
	function woo_get_settings() {
		// Set the default to a blank string
		$settings = array_fill_keys( $this->settings, '' );
		// Now set the more specific defaults
		return $settings;
	}

	function form($instance) {
		$instance = $this->woo_enforce_defaults( $instance );
		extract( $instance, EXTR_SKIP );
		
?>
		
		
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title (optional): </label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		

		<p>	The Form can be configured in the theme options under Subscriptions >> Mail List Subscription (Mailchimp) Widget - Home. 
		</p>


<?php  //print_r($instance['featured']);

	}
}
add_action( 'widgets_init', create_function( '', 'register_widget( "Simple_Mailchimp_Home_Subscribe" );' ) );

?>
