<?php
/*


*/

if ( ! defined( 'ABSPATH' ) ) exit;
/*---------------------------------------------------------------------------------*/
/* widget */
/*---------------------------------------------------------------------------------*/
class Simple_Mailchimp_Subscribe extends WP_Widget {
	var $settings = array( 'title', 'form' );

	function Simple_Mailchimp_Subscribe() {
		$widget_ops = array( 'description' => 'Display Mailchimp Signup Widget. Form can be modified in mailchimp api plugin and form values and options can be set in Theme Options' );
		parent::WP_Widget( false, 'Castle Simple Mailchimp Subscribe', $widget_ops );
	}

	function widget( $args, $instance ) {
		global $woo_options;
	
		/* Provide some defaults */
		$defaults = array( 'title' => '', 'text_before_form' => '', 'text_after_form' => '');
		$instance = wp_parse_args( (array) $instance, $defaults );	
			
		extract( $args );
		extract($instance);
		
		if (!empty($title)){ 
			$title = apply_filters('widget_title', $title);
			} else {
			$title  = $woo_options['woo_subscribe_widget_title'];
			}
			
		echo $before_widget;
		// insert the title
			// insert the title
			 echo '<h3>'.$woo_options['woo_subscribe_widget_title'].'</h3>'; 
			 
			// insert the image
			// show the image if it is set to show and it exists
			// $woo_options['woo_subscribe_widget_iwidth'] ? $thewidth = $woo_options['woo_subscribe_widget_iwidth'] : $thewidth = 200;
			
			$thealign  = (isset($woo_options['woo_subscribe_widget_ialign']) && !empty($woo_options['woo_subscribe_widget_ialign']) ? $woo_options['woo_subscribe_widget_ialign'] : 'aligncenter' );
			
			$thewidth = 250;
			if ( isset( $woo_options['woo_subscribe_widget_image'] ) && $woo_options['woo_subscribe_widget_image'] != '' ) {
				$theimage = $woo_options["woo_subscribe_widget_image"];
				woo_image('key=image&width='.$thewidth.'&noheight=true&force=true&id=123&class=thumbnail '.$thealign.'&src='.$theimage.''); 
			}
			// end image
			
			// before text
			if(!empty($woo_options['woo_subscribe_widget_text_before'])) { 
				echo '<div class="text-before-form">'.$woo_options['woo_subscribe_widget_text_before'].'</div>';		
			}
			echo '<div class="fix"></div>';
			echo output_mc_form('reg');
			echo '<div class="fix"></div>';
		
				// the after text

					if(!empty($woo_options['woo_subscribe_widget_text_after'])) { 
						echo '<div class="text-after-form">'.$woo_options['woo_subscribe_widget_text_after'].'</div>';		
					}

			// end after text

		echo $after_widget; 
	}

	function update($new_instance, $old_instance) {
		$new_instance = $this->woo_enforce_defaults( $new_instance );
		return $new_instance;
	}

	function woo_enforce_defaults( $instance ) {
		$defaults = $this->woo_get_settings();
		$instance = wp_parse_args( $instance, $defaults );
		$instance['title'] = strip_tags( $instance['title'] );
		if ( '' == $instance['title'] )
			$instance['title'] = 'Sign up for our newsletter';
		return $instance;
	}

	/**
	 * Provides an array of the settings with the setting name as the key and the default value as the value
	 * This cannot be called get_settings() or it will override WP_Widget::get_settings()
	 */
	function woo_get_settings() {
		// Set the default to a blank string
		$settings = array_fill_keys( $this->settings, '' );
		// Now set the more specific defaults
		return $settings;
	}

	function form($instance) {
		$instance = $this->woo_enforce_defaults( $instance );
		extract( $instance, EXTR_SKIP );
		
?>
		
		
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title (optional): </label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		

		<p>	The Form can be configured in the theme options under Subscriptions >> Mail List Subscription (Mailchimp) Widget. 
		</p>


<?php  //print_r($instance['featured']);

	}
}
add_action( 'widgets_init', create_function( '', 'register_widget( "Simple_Mailchimp_Subscribe" );' ) );

?>
